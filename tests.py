import unittest
import ctime
import datetime
import random

class TestBasic ( unittest.TestCase ):

    def setUp( self ):
        self.settings = ctime.Settings()
        self.settings.negative_offset          = datetime.timedelta( minutes = 10 )
        self.settings.time_begin_uncertain     = datetime.time( 4, 0 )
        self.settings.time_begin_reveal        = datetime.time( 8, 30 )
        self.settings.timedelta_reveal_timeout = datetime.timedelta( hours = 2 )


    def test_basic ( self ):
        random.seed(5)

        utime = ctime.UncertainTime( settings = self.settings )
        self.assertTrue( utime.state == 'TIME_EXACT' )

        someday = datetime.datetime( 2014, 07, 14 )

        dt_before = ctime.today_at( datetime.time( 3, 0 ), now = someday )
        utime.on_tick( now = dt_before )
        self.assertTrue( utime.state == 'TIME_EXACT' )
        self.assertEqual( utime.now( now = dt_before ) , dt_before )

        now = ctime.today_at( datetime.time( 4, 1 ), now = someday )
        utime.on_tick( now )
        self.assertTrue( utime.state == 'TIME_OFFSET' )
        self.assertEqual( utime.now( now = now ) , now - self.settings.negative_offset )
        self.assertFalse( utime.reveal_possible() )

        now = ctime.today_at( datetime.time( 8, 30 ), now = someday )
        utime.on_tick( now )
        self.assertTrue( utime.reveal_possible( now = now ) )

        now = ctime.today_at( datetime.time( 8, 32 ), now = someday )
        utime.on_tick( now )
        utime
        #self.assertTrue( utime.reveal_possible( now = now ) )

